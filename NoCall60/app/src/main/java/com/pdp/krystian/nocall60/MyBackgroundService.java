package com.pdp.krystian.nocall60;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

public class MyBackgroundService extends Service {
    public MyBackgroundService() {
    }

    private LocationListener locationListener;
    private LocationManager locationManager;
    // tworzenie obiektu typu AudioManager
    private AudioManager myAudioManager;

    double longitude;
    double latitude;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {

        myAudioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                longitude=location.getLongitude();
                latitude=location.getLongitude();

                Intent i = new Intent("location_update");
                i.putExtra("longitude",  location.getLatitude());
                i.putExtra("latitude", location.getLatitude());
                sendBroadcast(i);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        UpdateLocalization();

    }
    public void UpdateLocalization(){
        //noinspection MissingPermission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, locationListener);

    }

    public void GetDataAboutEventFromCalendar()
    {
        // kod trzeba przekopiować z MainActivity.java
    }


    public void IsStudentInClass(){

        // przyrównanie obecnej godziny z godziną z kalendarza
        // przyrównanie lokalizacji z lokalizacją z kalendarza
        //i wyciszenie lub odmutowanie
        //myAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        //myAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(locationManager != null){
            //noinspection MissingPermission
            locationManager.removeUpdates(locationListener);
        }
    }
}
